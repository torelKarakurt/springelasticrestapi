package torel.karakurt.SpringRestElasticJPA.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class JsonMapper {
    private JsonMapper(){}

    private static final Logger logger= LoggerFactory.getLogger(JsonMapper.class);

    public static JsonNode mapJsonNode(String data){
        try {
            return new ObjectMapper().readTree(data);
        } catch (IOException e) {
            if(logger.isErrorEnabled())
                logger.error(String.format("mapJson has failed with error: %s %n with data: %s %n ",e,data));
            return null;
        }
    }

    public static ObjectNode mapObjectNode(String aString) {
        try {
            return new ObjectMapper().readValue(aString, ObjectNode.class);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }
}
