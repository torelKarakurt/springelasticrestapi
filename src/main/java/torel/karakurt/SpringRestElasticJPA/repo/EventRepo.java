package torel.karakurt.SpringRestElasticJPA.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import torel.karakurt.SpringRestElasticJPA.model.Event;

import java.time.ZonedDateTime;
import java.util.List;

public interface EventRepo extends ElasticsearchRepository<Event, String> {
    Page<Event> findByTitle(String title, Pageable pageable);

    List<Event> findByAuditId(String auditId);

    Page<Event> findEventsByStartDateBeforeAndEndDateAfter(ZonedDateTime queryStartDate, ZonedDateTime queryEndDate, Pageable pageable);
}
