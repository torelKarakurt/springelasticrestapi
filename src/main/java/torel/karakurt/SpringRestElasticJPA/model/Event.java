package torel.karakurt.SpringRestElasticJPA.model;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.Id;
import java.time.ZonedDateTime;

@Document(indexName = "eventindex", type = "event")
public class Event {

    @Id
    private String id;
    private String title;
    private ZonedDateTime startDate;
    private ZonedDateTime endDate;
    private String auditId;

    public Event(){

    }
    public Event(String aId, String aTitle, ZonedDateTime aStartDate, ZonedDateTime aEndDate, String aAuditId){
        this.id = aId;
        this.title=aTitle;
        this.startDate = aStartDate;
        this.endDate = aEndDate;
        this.auditId = aAuditId;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public String getAuditId() {
        return auditId;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    @Override
    public String toString(){
        return "{ id: "+id+", title: "+title+", startDate: "+startDate.toString()+", endDate: "+endDate.toString()+
                ", auditId: "+auditId+"}";
    }

    public String toJsonString() {
        return "{ \"id\": \"" + id + "\", \"title\": \"" + title + "\", \"startDate\": \"" + startDate.toString() + "\", \"endDate\": \"" + endDate.toString() +"\", \"auditId\": \"" + auditId + "\"}";
    }
}
