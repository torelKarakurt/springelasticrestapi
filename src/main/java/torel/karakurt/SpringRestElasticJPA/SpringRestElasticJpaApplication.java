package torel.karakurt.SpringRestElasticJPA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestElasticJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestElasticJpaApplication.class, args);
	}

}
