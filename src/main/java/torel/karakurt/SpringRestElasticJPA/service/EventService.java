package torel.karakurt.SpringRestElasticJPA.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import torel.karakurt.SpringRestElasticJPA.model.Event;

import java.util.List;
import java.util.Optional;

public interface EventService {

    Event save(Event event);

    void delete(Event event);

    Optional<Event> findById(String id);

    Iterable<Event> findAll();

    Page<Event> findByTitle(String title, PageRequest pageRequest);

    List<Event> findByAuditId(String auditId);

    Page<Event> findAllViaDateQuery(String requestBody);
}
