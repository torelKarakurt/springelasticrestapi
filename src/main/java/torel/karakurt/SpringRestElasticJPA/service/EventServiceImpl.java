package torel.karakurt.SpringRestElasticJPA.service;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import torel.karakurt.SpringRestElasticJPA.model.Event;
import torel.karakurt.SpringRestElasticJPA.repo.EventRepo;
import torel.karakurt.SpringRestElasticJPA.utils.JsonMapper;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class EventServiceImpl implements EventService{
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    private EventRepo eventRepo;

    private static final String OFFSET = "offset";
    private static final String CHUNK = "chunk";
    private static final String SEARCH = "search";
    private static final String FILTERS = "filters";
    private static final String VIEWS = "views";
    private static final String START_DATE = "start_date";
    private static final String END_DATE = "end_date";
    private static final int DEFAULT_CHUNK_VALUE = 10;
    private static final int DEFAULT_OFFSET_VALUE = 0;

    @Autowired
    public void setEventRepo(EventRepo eventRepo) {
        this.eventRepo = eventRepo;
    }

    public Event save(Event event) {
        return eventRepo.save(event);
    }

    public void delete(Event event) {
        eventRepo.delete(event);
    }

    public Optional<Event> findById(String id) {
        return eventRepo.findById(id);
    }

    public Iterable<Event> findAll() {
        return eventRepo.findAll();
    }

    public Page<Event> findByTitle(String title, PageRequest pageRequest) {
        return eventRepo.findByTitle(title,pageRequest);
    }

    public List<Event> findByAuditId(String auditId) {
        return eventRepo.findByAuditId(auditId);
    }

    public Page<Event> findAllViaDateQuery(String requestBody) {
        String searchUUID = UUID.randomUUID().toString(); //for transactional logging
        ObjectNode jsonRequest = JsonMapper.mapObjectNode(requestBody);
        AtomicInteger offset = new AtomicInteger(DEFAULT_OFFSET_VALUE);
        AtomicInteger chunk = new AtomicInteger(DEFAULT_CHUNK_VALUE);
        try {
            Objects.requireNonNull(jsonRequest).fieldNames().forEachRemaining((String fieldName) -> {
                if (fieldName.equals(OFFSET) && !jsonRequest.get(OFFSET).isNull() && jsonRequest.get(OFFSET).isInt()) {
                    offset.set(jsonRequest.get(OFFSET).asInt());
                } else if (fieldName.equals(CHUNK) && !jsonRequest.get(CHUNK).isNull() && jsonRequest.get(CHUNK).isInt()) {
                    chunk.set(jsonRequest.get(CHUNK).asInt());
                }
            });
        } catch (Exception e) {
            logger.error("offset and chunk get failure, "+searchUUID+"requestBody of: "+requestBody+", error: "+e.getMessage(), e);
        }

        try {
            return eventRepo.findEventsByStartDateBeforeAndEndDateAfter(getStartDate(jsonRequest),getEndDate(jsonRequest), PageRequest.of(offset.get(),chunk.get(), Sort.by("startDate")) );
        } catch (Exception e) {
            logger.error("StartDate and EndDate get failure, "+searchUUID+"requestBody of: "+requestBody+", error: "+e.getMessage(), e);
        }
        return null;
    }

    private ZonedDateTime getStartDate(ObjectNode jsonRequest) throws NullPointerException{
        try {
            return ZonedDateTime.parse(jsonRequest.findValue(START_DATE).asText());
        } catch (NullPointerException e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }
    private ZonedDateTime getEndDate(ObjectNode jsonRequest) throws NullPointerException{
        try {
            return ZonedDateTime.parse(jsonRequest.findValue(END_DATE).asText());
        } catch (NullPointerException e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }
    
    
}
