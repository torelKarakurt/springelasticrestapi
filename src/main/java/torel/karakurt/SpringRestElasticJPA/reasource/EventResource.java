package torel.karakurt.SpringRestElasticJPA.reasource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import torel.karakurt.SpringRestElasticJPA.model.Event;
import torel.karakurt.SpringRestElasticJPA.service.EventService;

@RestController
@RequestMapping("/api")
public class EventResource {

    private final EventService eventService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    public EventResource(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping("/events")
    public ResponseEntity saveEvent(@RequestBody Event event) {
        try {
            return new ResponseEntity<>(eventService.save(event), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<>("ERROR:1001",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param aRequestJson a String representing a json object as described
     *                     * {
     *                     * 	"chunk":10,
     *                     * 	"offset":0,
     *                     * 	"start_date":"2018-01-16T16:47:00Z",
     *                     * 	"end_date": "2020-01-20T16:47:00Z"
     *                     * }
     * @return a ResponseEntity with the body containing a json ObjectNode with the events and meta data. Example:
     * * {
     * *     "events": [
     * *         {
     * *             "id": 15424,
     * *             "title": "string",
     * *             "startDate": "2019-02-05T12:45:45.597Z[UTC]",
     * *             "endDate": "2019-02-05T12:45:45.597Z[UTC]"
     * *         }
     * *     ],
     * *     "meta": {
     * *         "chunk": 10,
     * *         "offset": 0,
     * *         "totalEventCount": 1
     * *     }
     * * }
     */
    @PostMapping("/events/dateQuery")
    @ResponseBody
    public ResponseEntity dateQuery(@RequestBody String aRequestJson) {
        try {
            return new ResponseEntity<>(eventService.findAllViaDateQuery(aRequestJson), HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<>("ERROR:1001",HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
