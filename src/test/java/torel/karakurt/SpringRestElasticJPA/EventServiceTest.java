package torel.karakurt.SpringRestElasticJPA;

import org.apache.http.HttpHost;
import org.aspectj.lang.annotation.Before;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import torel.karakurt.SpringRestElasticJPA.model.Event;
import torel.karakurt.SpringRestElasticJPA.service.EventService;

import java.io.IOException;
import java.time.ZonedDateTime;

import static org.hamcrest.MatcherAssert.assertThat;


@SpringBootTest(classes = SpringRestElasticJpaApplication.class)
public class EventServiceTest {

    @Autowired
    private EventService eventService;


    private RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(new HttpHost("localhost", 9200, "http")));
    @Before("beforeAllTests")
    public void before() throws IOException {
        client.indices().delete(new DeleteIndexRequest("testeventindex"), RequestOptions.DEFAULT);
        client.indices().create(new CreateIndexRequest("testeventindex"), RequestOptions.DEFAULT);
    }

    @Test
    public void testSave() {
        Event event = new Event("1001", "basicTitle", ZonedDateTime.now().plusDays(1), ZonedDateTime.now().plusDays(2), "2001");
        Event testEvent = eventService.save(event);

        assertThat("objects are equal",event.getTitle().equals(testEvent.getTitle()));
        //more would be here
    }

}
